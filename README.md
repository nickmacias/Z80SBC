# Z80 Pocket-Sized Single Board Computer
[![Hackaday Page](https://img.shields.io/badge/Blog-Hackaday-red)](https://hackaday.io/project/177345-pocket-sized-imsai-style-z80-board)

<img src=img/fullBoard.png width="300">
(click to enlarge)

A single-board Z80 processor, designed in the spirit of the old Imsai 8080 system. The goal is a Z80-based system which is highly accessible from front panel controls and indicators. The used has the ability to view and modify memory; step the CPU; examine bus signals; apply user input and view user output; and other low-level operations.

The [current version of the system](https://hackaday.io/project/177345-pocket-sized-imsai-style-z80-board/log/190680-status-update-pretty-much-done) includes a WiFi interface and headers for directly accessing the Z80 signals. The CLI includes a disassembler, and has hooks for a binary loader. There's also flash memory storage for saving multiple copies of SRAM.

Check out the [user manual](doc/usermanual/manual.pdf) for more details on the current version.

# Table of contents
* [License](#license)
* [Background](#background)
* [Overview](#overview)
* [Features](#features)
* [Hardware](#hardware)
* [Software](#software)
* [Applications](#applications)
* [Next Steps](#next-steps)


## License

I make no claim of ownership for any material herein that is not my own. For example, I have uploaded a PDF scan of my notes for the board, which includes a few pages from datasheets that are marked as copyrighted by their respective owners.

<img src="img/by-sa.png" width="200">

For all **original** work contained herein, license is given under the Creative Commons Share-alike - Attribution 4.0 License. You are free to use/modify/redistribute this work without charge, provided you follow the license terms. See
[https://creativecommons.org/licenses/by-sa/4.0/legalcode](https://creativecommons.org/licenses/by-sa/4.0/legalcode) for more details.

This work is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

## Background
In the late 70s/early 80s, I learned about microcomputers in part by using an [Imsai 8080](https://en.wikipedia.org/wiki/IMSAI_8080). This featured an Intel 8080 that would execute code from internal memory, but the memory was viewable and modofiable via front panel controls. To use the system, one had to write [assembly language code](https://en.wikipedia.org/wiki/Intel_8080#Example_code), assemble it by hand (because we didn't have an assembler!), and then, given the binary machine code, toggle it into the machine's memory.

The process was cumbersome:
* enter the starting address on a set of 16 paddle switches
* press the "examine" switch to move the system's address to the given value
* enter the first instruction on 8 paddle switches
* press "deposit" to store that byte at the given location
* enter the next instruction byte on 8 paddle switches
* press "desosit next" to advance the address and save this byte

The above two steps were repeated until the entire program was loaded into memory. Following that, the initial address could be re-entered, and then the STEP or (if you were very brave) RUN switch would cause the CPU to begin executing the entered program.

Common problems included:
* making a mistake in any of the inputs;
* forgetting to return to the initial location before running;
* programming errors; such as not specifying what the CPU should do at the end of your program;

and so on.

Remember, this is machine code, so almost any input will constitute a legal program, which will be happily executed by the CPU. If a mistake is made, and you stop the CPU after a second, numerous instructions will already have executed, perhaps corrupting the program you so carefully entered. You'd then need to go back and re-examine memory, re-enter any changed instructions, and so on.

A classic issue was finding an error in your logic, and needing to insert a new instruction in-between two existing ones. This would change the address of subsequent instructions, thus changing the coding of any jump instructions that address code later in the program.

All of which is to say, **this was a lot of fun!** but it required patience, care, and sometimes it felt like a bit of luck. It also inspired one quickly to develop things like bootloaders or keyboard interfaces :)

This was all also highly educational: there's no surer way to learn the basics of a system that by working with it at its lowest level. If you want to design and build cars, you can spend some time driving them, but that will teach you little about how they work. Eventually, you need to not only look at the engine, but take it apart, look at the seals, look at how it wears, and then start making changes, running tests, and so on. Same with understanding computers.

## Overview

I thought it would be fun reproduce this experience (to some degree) in a modern platform, and make this available to others to experiment with and learn from. I also wanted to get some experience with PCB design and surface mount assembly. Finally, I was just looking for a big-ish project I could sink into for a few weeks. This SBC is the result.

While using the paddle switches on the Imsai was pretty fun, using small rocker, slide or piano switches in a DIP is not as fun. It's fine for a bit of interaction, but once you've got the hang of it, you start to want something a *little easier*. This was the motivation for adding a command line interface (CLI).

Apparently this is a slippery slope. Once I had a CLI, I quickly wanted a wireless interface. The [ESP-01](http://www.microchip.ua/wireless/esp01.pdf) is an inexpensive and simple way to add WiFi to a project, so that's part of the board, which will switch pretty seamlessly between USB- and WiFi-connectivity to the CLI.

## Features
Main features of the system include:
* 4 MHz Z80
* 64Kx8 static RAM
* rechargable LiPo with enough capacitry to run for (typically) 4-5 hours
* system can run from USB power while also charging the internal battery
* ability to read and modify RAM from front panel LEDs and switches
* system can be run, stopped of single-stepped from the front panel
* ability to single step the CPU with three different granularities (single clock cycle; single memory cycle; and single instruction)
* access to interrupt inputs from front panel
* multi-colored LEDs (YAY!!!)
* access to Z80 bus and control signals via 2 x 20 x 2.54 mm header
* Full command line interface (CLI) for access to front panel functions via a terminal program
* Additional CLI features include disassembly of machine code, bulk loading of binary data, and examination of blocks of memory
* ATMeag328 processor for running the CLI and monitoring switches (mostly fly-by-wire)
* Wireless access to CLI (can be turned off to save power)

The [user manual](doc/usermanual/manual.pdf) describes the use of the system, including examples with screenshots.

## Hardware

<img src=img/breadboard.png width="400">

The system was originally developed on a set of breadboards to work out the wiring and such. You can find my (mostly disorganized) notes [here](doc/originalNotes.pdf). 

The current version of the board is a two-sided, 4-layer PCB with all parts hand-soldered. For the most part, devices (ICs and passives) are on the back, and switches and LEDs are on the front. The schematic and PCB were developed using [Eagle PCB software](http://eagle.autodesk.com/). Eagle is, in my opinion, a great piece of software. The interface is unlike anything else I've used (except maybe the [Magic VLSI Layout Tool](http://opencircuitdesign.com/magic/) from the 80s), but once you get used to it it feels very natural. Unfortunately, Eagle was sold recently, and now requires a monthly fee to do 4-layer designs. Monthly licensing seems to actually involve a monthly subscription to something called "Fusion 360," which includes Eagle "for free." I'm looking at [KiCad](https://kicad.org/) as a possible upgrade.

<img src="img/PCBTop.png" width="200">
<img src="img/schematic.png" width="200">
<img src="img/PCBBottom.png" width="200">

(click for larger versions)

You can also look at the [manual pick and place](img/PCBPnP.pdf) image I've been using to assemble the boards.

Be aware that this current version (A045) includes an error in the generation of the FF# signal, which should be based on A7-A0 instead of D7-D0. This can be patched on the finished board by wiring a jumper between the output (pin 8) of IC1and the A7 address signal (accessible from a nearby via). This bypasses the FF# logic, and causes *all* ports from 0x00-0x7F to come from/go to the front panel switches/LEDs.

<img src="img/boardFix.png" width="300">

[Here are the Eagle files](eagle) including a [parts list](eagle/BoM.txt).
**Note that there are some errors in here: the crystal is certainly wrong, and should be 16 MHz not 32.768K. Also, C16 and C17 are 22 uF not 22 pF.** I have corrected these in the parts list, but not on the schematic.

<img src="case/case.png" width="250">

The case is [3-D printed](case/Z80BoxV100.stl), and includes cutaways for the bottom of the header, as well as for two passives on the underside of the board. The PCB slides in and needs to be pushed snug against the bottom with a bit of force. There's a hole in the bottom that lines up with a reset button for the ATMega328. The front plate has cutaways for the two power switches and the USB connector, and slides in from the top. Once in place, it seems to fit pretty securely.

## Software

The system is powered by an ATMega328 configured using the Arduino ecosystem, including [MCUdude's Minicore](https://github.com/MCUdude/MiniCore) (whose GIT page is awesome, and whose README I ripped off as a template for my own - thanks!). The [latest code](software/latest.ino) fits in the 328 using 76% of program storage and 48% of dynamic memory. There's room to grow, but I've gotten agressive with storing strings, including writing a function to let me use F("...") style strings with functions like sprintf.

```// can make normal (char* style) strings using mycvt(F("hahaha"))
char myout[32];
char *mycvt(const __FlashStringHelper* in)
{
  String s=String(in);
  int i;
  for (i=0;i<s.length();i++){
    myout[i]=s.charAt(i);
  }
  myout[i]='\0';
  if (i >= 32){
    Serial.print(in);
    Serial.println(F("*** LINE TOO LONG TO CVT! ***"));
  }
  return(myout);
}
```

The ESP-01 is running a [simple program](software/Remote.ino), built using the [WiFiManager](https://github.com/tzapu/WiFiManager) library. It's a basic conduit between the ESP server's I/O and the serial I/O pins of the ESP-01. Switching betweenb WiFi and USB connectivity is handled in hardware, and is triggered via the WiFi power switch.

There's a [loader program](software/loader) which will let you bulk load binary data into the board using the super secret "load" command (documented in the [user manual](doc/usermanual/manual.pdf)). The loader uses [Fazecast, Inc.'s jSerialComm](https://fazecast.github.io/jSerialComm/) library, which appears to be published under GPL [here](https://github.com/Fazecast/jSerialComm/blob/master/COPYING).

Regardless, the code under [software/loader/Z80](software/loader/Z80) should give you an idea how to write your own loader if needed. This code also has a rudimentary terminal front-end, but the scrolling has issues: one version works well under Linux but flashes madly under Windows; another looks good in Windows but does not auto-scroll in Linux. Life goes on. For loading binary code, it seems to work well enough :)

## Applications

The most ambitious project I've done so far with this board was installing a CP/M 2.2 system. This involved:

* downloading assembly code for CP/M 2.2;
* designing an [interface circuit](app/CPM/interface.png) to allow an Arduino (nano) to serve as a console and floppy disk controller (one part of this needs to be changed to be [edge-triggered](app/CPM/edgeTriggering.png));
* designing a simple [communication protocol](app/CPM/protocol.png)/[handshake](app/CPM/consolHandshake.png) between the Z80/s I/O system and the nano;
* patching the CP/M [BIOS](app/CPM/bios.asm) to use this comm protocol;
* building synthetic floppies and [initializing](app/CPM/init.c) them to be empty (0xE5 in every byte);
* loading the system pieces into RAM and getting CP/M booted;
* writing [code](app/CPM/saveOSToDisk.asm) to copy upper memory to floppy disk a/track 00;
* writing a [bootloader](app/CPM/readOSFromDisk.asm) to read track 0 into memorynd then;
* interfacing a keyboard and small display to the nano and writing an [Arduino program](app/CPM/Z80CPM.ino) to respond to Z80 commands.

Once CP/M was easily bootable, I was able to download various programs, [convert](app/CPM/com2hex.c) them from .COM to .HEX, upload them to RAM with the [loader](software/loader), and then save them with the CP/M *save* command. This was a ridiculous amount of fun :)

## Next Steps

This has been a lot of fun, I've learned a lot, and I'm sending these boards to a few friends who will hopefully enjoy them. I don't have any definitive plans myself for where to go next. Getting CP/M up was great fun, and it works really well with, say, a laptop hooked up as the KB/CRT; but the idea of a more self-contained system is appealing. I'm looking at [FabGL](https://github.com/fdivitto/fabgl) which seems awesome! Can run it on an ESP-32 and [apparently](https://www.instructables.com/ESP32-Basic-PC-With-VGA-Output/) drive a VGA, keyboard, and SD card. I hope to follow that [instructable](https://www.instructables.com/ESP32-Basic-PC-With-VGA-Output/) for the basic configuration, but tie in my interface circuit etc, and basically use this as a terminal+floppy disk controller.

Meanwhile, I'm thinking about educational options for this. I could see building a microprocessor/operating system/interfacing/etc. course around this. Let students design a basic OS, give them an interface protocol to follow, and work towards a boot loader/etc. Or give them a simple OS and have them develop a BIOS. Or they could work on the hardware/interface side. Or all of the above!

What I find appealing about this is that it's powerful enough to run a large-scale operating system or dedicated application, yet it still exposes the lowest-level details (decoding memory and control signals to respond to I/O requests, for example). It's well-suited to real-time programming. The assembly language is simple enough to get started with quickly, yes has CISC-like instructions (conditional jumps, block moves, etc.) It feels like a nice sweet spot in the architecture space, and I think it would be fun to teach a course around it.
