#include <ESP8266WiFi.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>
//#include <Software.h>

//SoftwareSerial mySerial(5,6); // RX, TX

WiFiServer server(8081);
WiFiClient client;

void setup()
{
    Serial.begin(115200);
    WiFiManager wifiManager;
    wifiManager.setSTAStaticIPConfig(IPAddress(192,168,0,88), IPAddress(192,168,0,88), IPAddress(255,255,255,0));

    //fetches ssid and pass from eeprom and tries to connect
    wifiManager.autoConnect("Z80 AutoConnectAP");
    server.begin();
}

int connected=0; // set when we're connected (clear after disconnect)

void loop()
{
  if (!connected){
    client=server.available();
    if (!client) return;
    connected=1; // record that we're connected
    return;
  }

// we've been connected
  if (!client.connected()){ // disconnected
    connected=0;return;
  }

// still connected :)
  char buffer[120];
  char c;
// read up to 64 bytes from client
  int avail=client.available();
  if (avail > 64) avail=64;

  for (int i=0;i<avail;i++){
    buffer[i]=client.read();
  }
  buffer[avail]='\0'; // just in case :)

// use availableforwrite to see how many bytes we can send to serial port?
  mySend(buffer,avail); // send to Z80
  
// read from Z80 tx pin, send to client
// use Serial.available() to see how many bytes are available; read them (up to 64 bytes); and send to client
  avail=Serial.available();
  if (avail > 256) avail=256;
  for (int i=0;i<avail;i++){
    c=myRcv();
    if (c==0) return; // ???
    client.write(c);
  }
}

// send to serial port (Z80)
void mySend(char *c,int len)
{
  Serial.write(c,len);
}

// read from serial port (Z80)
char myRcv()
{
  char c=Serial.read();
  //if (c==0) Serial.println("Read a 0 from Z80");
  return(c);
}
