#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

int main(int argc, char **argv)
{
  int fp;
  char c;
  int loc=0x100; // next loc to save

  if (argc != 2){
    fprintf(stderr,"USAGE: %s comfile",argv[0]);
    return(1);
  }

  fp=open(argv[1],O_RDONLY);
  if (fp < 0){
    fprintf(stderr,"ERROR: Cannot open %s\n",argv[1]);
    return(1);
  }

  char buffer[2];
  while (1==read(fp,buffer,1)){
    if (loc%16==0){ // start a new line
      if (loc != 0x100) printf("\n");
      printf(":10%02X%02X00",(loc>>8)&0xff,(loc&0xff));
    }
    printf("%02X",buffer[0]&0xff);
    ++loc;
  }
  while (loc%16 != 0){
    printf("00");++loc; // padding
  }
  printf("\n:00000000\n");

  fprintf(stderr,"bytes: %d pages: %d\n",(loc-256),loc/256); // rounds up!
}
